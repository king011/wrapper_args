### 1.0.1
- fix subcommand height > 1 execute height\[1\]
- parseAndExecute return execute code

## 1.0.0

- Initial version, created by Stagehand
