import 'package:wrapper_args/wrapper_args.dart';
import 'dart:io';

void main(List<String> arguments) {
  // create root command
  final root = _root();

  // create subcommand
  root.addCommand([
    _server(),
    _client(),
  ]);

  // parse and execute
  final code = root.parseAndExecute(arguments);

  // set exit code
  exitCode = code ?? 0;
}

Command _root() {
  final parser = ArgParser()
    ..addFlag(
      "help",
      abbr: "h",
      negatable: false,
      help: "Print this usage information",
    )
    ..addFlag(
      "version",
      abbr: "v",
      negatable: false,
      help: "Print the version",
    );
  return Command(
    help: "Usage: wrapper_args [<subcommand>] [<options>]",
    parser: parser,
    execute: (command, results) {
      if (results["help"]) {
        print("${command.usage}");
      } else if (results["version"]) {
        print("version 1.0.0");
      }
    },
  );
}

Command _server() {
  final parser = ArgParser()
    ..addFlag(
      "help",
      abbr: "h",
      negatable: false,
      help: "Print this usage information",
    )
    ..addOption(
      "listen",
      abbr: "l",
      help: "tcp listen address",
    );
  return Command(
    name: "server",
    help: "simulator server",
    parser: parser,
    execute: (command, results) {
      if (results["help"]) {
        print(command.usage);
      } else {
        String str = results['listen'];
        if (str == null || str.isEmpty) {
          return 1;
        }
        print("server work at $str");
      }
    },
  );
}

Command _client() {
  final parser = ArgParser()
    ..addFlag(
      "help",
      abbr: "h",
      negatable: false,
      help: "Print this usage information",
    )
    ..addOption(
      "target",
      abbr: "t",
      help: "connect target",
    );
  return Command(
    name: "client",
    help: "simulator client",
    parser: parser,
    execute: (command, results) {
      if (results["help"]) {
        print(command.usage);
      } else {
        String str = results['target'];
        if (str == null || str.isEmpty) {
          return 2;
        }
        print("connet ${results['str']} ...");
      }
    },
  )..addCommand(
      [
        _clientHttp(),
      ],
    );
}

Command _clientHttp() {
  final parser = ArgParser()
    ..addFlag(
      "help",
      abbr: "h",
      negatable: false,
      help: "Print this usage information",
    )
    ..addOption(
      "target",
      abbr: "t",
      help: "connect target",
    );

  return Command(
    name: "http",
    help: "simulator http",
    parser: parser,
    execute: (command, results) {
      if (results["help"]) {
        print(command.usage);
      } else {
        String str = results['target'];
        if (str == null || str.isEmpty) {
          return 3;
        }
        print("http connet $str ...");
      }
    },
  );
}
