import 'dart:core';
import 'dart:math' as math;
import 'package:args/args.dart';

class Command {
  final Map<String, Command> _keys;
  final String name;
  final ArgParser parser;
  final String help;
  final String longHelp;
  int Function(Command, ArgResults) execute;
  Command({
    this.name,
    this.help = "",
    this.longHelp = "",
    this.parser,
    this.execute,
  })  : assert(name != null),
        assert(help != null),
        assert(longHelp != null),
        assert(parser != null),
        assert(execute != null),
        _keys = Map<String, Command>();

  addCommand(List<Command> cmd) {
    assert(cmd != null);
    cmd.forEach((cmd) {
      assert(cmd != null);

      final name = cmd.name;
      assert(name != null);

      if (_keys.containsKey(name)) {
        throw AssertionError("subcommand $name already exists");
      }
      _keys[name] = cmd;
      parser.addCommand(name, cmd.parser);
    });
  }

  int parseAndExecute(List<String> arguments) {
    final results = parser.parse(arguments);
    return _execute(this, results);
  }

  static int _execute(Command command, ArgResults results) {
    if (results.command == null) {
      return command.execute(command, results);
    } else {
      final name = results.command.name;
      final executable = command._keys[name];
      if (executable == null) {
        throw UnsupportedError("not found subcommand : $name");
      } else {
        return _execute(executable, results.command);
      }
    }
  }

  String _help() {
    if (longHelp != null && longHelp.isNotEmpty) {
      return longHelp;
    }
    return help;
  }

  String _usage;
  String get usage {
    if (_usage != null) {
      return _usage;
    }
    final buffer = StringBuffer();
    final help = _help();
    if (help != null && help.isNotEmpty) {
      if (help.endsWith("\n")) {
        buffer.write(help);
      } else {
        buffer.write(help + "\n");
      }
    }
    if (parser.options != null && parser.options.isNotEmpty) {
      buffer.write('''\nOptions:
  ${parser.usage.split("\n").join("\n  ")}
''');
    }

    if (_keys.isNotEmpty) {
      buffer.write("\nSubcommands:\n");
      int max = 0;
      _keys.forEach((name, _) {
        max = math.max(max, name.length);
      });
      _keys.forEach((name, command) {
        buffer.write("  ${name.padRight(max + 4)}${command.help}\n");
      });
    }

    _usage = buffer.toString();
    return _usage;
  }
}
