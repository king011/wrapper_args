/// Parses raw command-line arguments into a set of options and values.
///
/// Wrapper package:args/args.dart for my style
library wrapper_args;

export 'package:args/args.dart';
export './src/command.dart';
