A library for Dart command-line.

Parses raw command-line arguments into a set of options and values. Wrapper package:args/args.dart for my style

![](images/args.png)

## Usage

A simple usage example:

```dart
import 'package:wrapper_args/wrapper_args.dart';

main(List<String> arguments) {
  // create root command
  final root = _root();

  // create subcommand
  root.addCommand([
    _server(),
    _client(),
  ]);

  // parse and execute
  root.parseAndExecute(arguments);
}

Command _root() {
  final parser = ArgParser()
    ..addFlag(
      "help",
      abbr: "h",
      negatable: false,
      help: "Print this usage information",
    )
    ..addFlag(
      "version",
      abbr: "v",
      negatable: false,
      help: "Print the version",
    );
  return Command(
    help: "Usage: wrapper_args [<subcommand>] [<options>]",
    parser: parser,
    execute: (command, results) {
      if (results["help"]) {
        print("${command.usage}");
      } else if (results["version"]) {
        print("version 1.0.0");
      }
    },
  );
}

Command _server() {
  final parser = ArgParser()
    ..addFlag(
      "help",
      abbr: "h",
      negatable: false,
      help: "Print this usage information",
    )
    ..addOption(
      "listen",
      abbr: "l",
      help: "tcp listen address",
    );
  return Command(
    name: "server",
    help: "simulator server",
    parser: parser,
    execute: (command, results) {
      if (results["help"]) {
        print(command.usage);
      } else {
        print("server work at ${results['listen']}");
      }
    },
  );
}

Command _client() {
  final parser = ArgParser()
    ..addFlag(
      "help",
      abbr: "h",
      negatable: false,
      help: "Print this usage information",
    )
    ..addOption(
      "target",
      abbr: "t",
      help: "connect target",
    );
  return Command(
    name: "client",
    help: "simulator client",
    parser: parser,
    execute: (command, results) {
      if (results["help"]) {
        print(command.usage);
      } else {
        print("connet ${results['target']} ...");
      }
    },
  );
}
```
